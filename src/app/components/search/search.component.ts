import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent{

  artistas: any[] = [];
  loading: boolean;
  constructor( private _spotifyService: SpotifyService) { }

  buscar( termino: string ){
    this.loading = true;
    this._spotifyService.getArtistas( termino )
    .subscribe( (response: any) => {
      this.artistas = response;
      this.loading = false;  
    });
  }

}
