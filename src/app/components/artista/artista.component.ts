import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html'
})
export class ArtistaComponent {

  artista: any = {};
  loading: boolean;
  topTracks: any[] = [];
  multiplos3: any[] = [];
  multiplos5: any[] = [];
  multiplos7: any[] = [];

  tres: number = 3;
  cinco: number = 5;
  siete: number = 7;

  resto: number;

  constructor( private router: ActivatedRoute,
                private _spotifyService: SpotifyService) {
    this.loading = true;
    this.router.params.subscribe( params => {
      this.getArtista( params['id'] );
      this.getTopTracks( params['id'] );
    });
  }

  getArtista( id: string){
    this.loading = true;
    this._spotifyService.getArtista( id )
    .subscribe( artista => {
      console.log(artista);
      this.artista = artista;
      this.loading = false;
    });
  }

  getTopTracks( id: string ){
    this._spotifyService.getTopTracks( id )
    .subscribe( topTracks => {
      console.log(topTracks);
      this.topTracks = topTracks;
    });
  
  }

  

  


}
