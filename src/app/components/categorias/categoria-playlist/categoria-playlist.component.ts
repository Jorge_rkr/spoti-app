import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-categoria-playlist',
  templateUrl: './categoria-playlist.component.html',
  styles: [
  ]
})
export class CategoriaPlaylistComponent {

  loading: boolean;
  playlistCategorias: any[] = [];

  constructor(private activatedRoute: ActivatedRoute,
    private _spotifyService: SpotifyService,
    private router: Router) {
    this.loading = true;
    this.activatedRoute.params.subscribe(params => {
       this.getPlaylists( params['categoriaId'] );
    });
  }

  getPlaylists( categoriaId: string ){
    this.loading = true;
    this._spotifyService.getCategoriesPlaylist( categoriaId )
    .subscribe( response => {
      this.playlistCategorias = response; 
      this.loading = false;
    });
  }

  verPlaylist( playlistData: any[] ){ 
    this._spotifyService.setCategoriaData( playlistData );
    this.router.navigate(['/playlists', playlistData['id'], 'tracks']);
  }

}
