import { Input } from '@angular/core';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html'
})
export class CategoriasComponent {
  
  @Input() items: any[] = [];
  @Input() loading: boolean;

  constructor( private router: Router) { 
  }

  verPlaylists( categoriaId: any ){ 
    this.router.navigate(['/categories', `${categoriaId}`, 'playlists']);
  }



}
