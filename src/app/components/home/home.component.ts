import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  nuevasCanciones: any[] = [];
  categorias: any[] = [];
  loading: boolean;
  error: boolean = false;
  mensajeError: string;



  constructor(private _spotifyService: SpotifyService) {
    this.loading = true;
    this.error = false;

    this._spotifyService.getNewReleases()
      .subscribe((response: any) => {
        this.nuevasCanciones = response;
        this.loading = false;
      }, (errorServicio) => {
        this.loading = false;
        this.error = true;
        this.mensajeError = errorServicio.error.error.message;
      });

    this._spotifyService.getCategories()
      .subscribe((response: any) => {
        this.categorias = response;
        this.loading = false;
      }, (errorServicio) => {
        this.loading = false;
        this.error = true;
        this.mensajeError = errorServicio.error.error.message;
      });
  }





}
