import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html'
})
export class PlaylistComponent {

  playlist: any[] = [];
  loading: boolean;
  categoria: any[] = [];
  imagen: any[] = [];
  nombreCategoria: string;
  descripcionCategoria: string;
  categoriaId: string;

  constructor(private activatedRoute: ActivatedRoute,
    private _spotifyService: SpotifyService) {
      this.loading = true;
    this.activatedRoute.params.subscribe(params => {
      this.getPlaylist(params['playlistId']);
    });

    this.categoria = this._spotifyService.getCategoriaData();
    this.imagen = this.categoria['images'];
    this.nombreCategoria = this.categoria['name'];
    this.descripcionCategoria = this.categoria['description'];
    this.categoriaId = this.categoria['id'];
  }

  getPlaylist( playlistId: string ){
    this.loading = true;
    this._spotifyService.getPlaylistItems( playlistId )
    .subscribe( response => {
        this.playlist = response;
        this.loading = false;
    });
  }

}
