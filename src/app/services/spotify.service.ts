import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  categoria: any[] = [];

  constructor(private http: HttpClient) {
    console.log('Spotify service listo');

  }

  getQuery( query: string){
    const url = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDcgVUnPLyCnFY-ZB0awoVCXID28NqySZyAwIUmyiWrNVLli004UDrLbKICpw4nylhHEci0RrZQgJC9Tr0'
    });

    return this.http.get( url, { headers });
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases?limit=20')
    .pipe(map(data => data['albums'].items ));
  }

  getArtistas( termino: string ) {
    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map(data => data['artists'].items ));  
  }

  getArtista( id: string ) {
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracks( id: string ) {
    return this.getQuery(`artists/${ id }/top-tracks?country=MX`)
      .pipe( map(data => data['tracks'] ));  
  }

  getCategories() {
    return this.getQuery(`browse/categories?country=MX&locale=es_ES&limit=10`)
    .pipe( map( data => data['categories'].items));
  }

  getCategoriesPlaylist( categoriaId: string ) {
    return this.getQuery(`browse/categories/${categoriaId}/playlists?country=MX&limit=5`)
    .pipe( map( data => data['playlists'].items));
  }

  getPlaylistItems( playlistId: string ){
    return this.getQuery(`playlists/${playlistId}/tracks?market=MX&limit=10`)
    .pipe( map( data => data['items']) );
  }

  getCategoriaData (): any [] {
    return this.categoria;
  }

  setCategoriaData ( categoria ): void {
    this.categoria = categoria;
  }
}
