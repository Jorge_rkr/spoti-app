import { Routes } from '@angular/router';
import { ArtistaComponent } from './components/artista/artista.component';
import { CategoriaPlaylistComponent } from './components/categorias/categoria-playlist/categoria-playlist.component';
import { HomeComponent } from './components/home/home.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { SearchComponent } from './components/search/search.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: 'artist/:id', component: ArtistaComponent },
    { 
        path: 'categories/:categoriaId',
        children: [
            { path: 'playlists', component: CategoriaPlaylistComponent },
        ]
     },
    { 
        path: 'playlists/:playlistId',
        children: [
            { path: 'tracks', component: PlaylistComponent }
        ]
     },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];