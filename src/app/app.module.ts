import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ROUTES } from './app.routes';

import { TarjetasComponent } from './components/tarjetas/tarjetas.component';
import { LoadingComponent } from './components/shared/loading/loading.component';

// Pipes
import { NoimagePipe } from './pipes/noimage.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { FooterComponent } from './components/shared/footer/footer.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { CategoriaPlaylistComponent } from './components/categorias/categoria-playlist/categoria-playlist.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { TableTracksComponent } from './components/table-tracks/table-tracks.component';

// Importar rutas

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistaComponent,
    NavbarComponent,
    NoimagePipe,
    TarjetasComponent,
    LoadingComponent,
    DomseguroPipe,
    FooterComponent,
    CategoriasComponent,
    CategoriaPlaylistComponent,
    PlaylistComponent,
    TableTracksComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot( ROUTES, { useHash:true } )
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
